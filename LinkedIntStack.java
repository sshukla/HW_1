/**
 * Stacks of integers using linked lists.
 * 
 * @author Shail Shukla
 */
public class LinkedIntStack implements IntStackInterface {

    private class Node {
        public int value;
        public Node next;
        public Node(int value, Node next) {
            this.value = value;
            this.next = next;
        }
    }

    private Node _head;
    
    /**
     * Constructs an empty Stack.
     */
    public LinkedIntStack() {
        _head = null;
    }
        

    /**
     * Returns true if stack is empty, false otherwise.
     * @return whether stack is empty
     */
    public boolean isEmpty() {
        if(_head == null)
            return true;
        return false;
    }

    
    /**
     * Pushes specified integer onto the top of the stack.
     * @param i the string to be pushed onto the stack
     */
    public void push(int i) {
        if (_head != null) {
            Node node = new Node(i, _head);
            _head = node;
        } else {
            _head = new Node(i, null);
        }
    }

    
    /**
     * Removes and returns integer currently sitting on top of stack,
     * assuming stack is not empty. (If it is empty, it generates a
     * "stack underflow" error.)
     * @return integer on top of stack that is removed
     */
    public int pop() {
        if (_head != null) {
            int i = _head.value;
            _head = _head.next;
            return i;
        }else{
            throw new RuntimeException("Stack is Empty!");
        }
    }


    /**
     * Returns string representation of stack.
     * @return string representation of stack
     */
    String stringRep() {
        String s = "";
        Node node = _head;
        while(node != null){
            s = s + node.value + " ";
            node = node.next;
        }
        return s;  
    }
    
}
