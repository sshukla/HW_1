// Data Structures & Algorithms
// Spring 2018
// HW1: stacks and linked lists
// postfix calculator (starter code)


import java.util.Scanner;


/**
 * @author Shail Shukla
 */
public class Evaluate {

    public static void main(String[] args)  {
        Scanner scanner = new Scanner(System.in);
        int result = evaluate(scanner.nextLine());
        System.out.println("\nevaluates to: " + result);
    }

    
    /**
     * Returns the "result" of computing a sequence of
     * postfix-calculator instructions.
     * @param source white-space separated sequence of postfix instructions
     * @return result of evaluating postfix instructions
     */
    public static int evaluate(String source) {
        Scanner scanner = new Scanner(source);
        LinkedIntStack stack = new LinkedIntStack();
        while (scanner.hasNext()) {
            System.out.println("" + stack.stringRep());
            if (scanner.hasNextInt()) {
                // ... do something with scanner.nextInt() ...
            } else {
                String s = scanner.next();
                if (s.equals("+")) {
                    int a = stack.pop();
                    int b = stack.pop();
                    stack.push(b + a);
                } else if (s.equals("-")) {
                    int a = stack.pop();
                    int b = stack.pop();
                    stack.push(b - a);
                } else if (s.equals("*")) {
                    int a = stack.pop();
                    int b = stack.pop();
                    stack.push(b * a);
                } else if (s.equals("/")) {
                    int a = stack.pop();
                    int b = stack.pop();
                    stack.push(b / a);
                } else if (s.equals("%")) {
                    int a = stack.pop();
                    int b = stack.pop();
                    stack.push(b % a);
                }  else if (s.equals("clr")) {
                    while(stack.isEmpty() == false){
                        stack.pop();
                    }
                    stack.push(0);
                } else if (s.equals("dup")) {
                    int a = stack.pop();
                    stack.push(a);
                    stack.push(a);
                }else if (s.equals("prn")) {
                    System.out.println("" + stack.stringRep());
                } else if (s.equals("swap")) {
                    int a = stack.pop();
                    int b = stack.pop();
                    stack.push(a);
                    stack.push(b);
                } else if (s.equals("ifz")) {
                    int a = stack.pop();
                    if(a == 0){
                        stack.pop();
                    } else if(a == 1) {
                        int b = stack.pop();
                        stack.pop();
                        stack.push(b);
                    }else{
                        stack.push(a);
                    }
                } else {
                        throw new RuntimeException("invalid postfix command: " + s);
                } 
            }
        }
        return stack.pop();
    }
}
