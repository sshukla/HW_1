/**
 * Stacks of strings using resizable arrays.
 * 
 * @author Shail Shukla
 */
public class ArrayStringStack implements StringStackInterface {

    
    private String[] _stringStack;
    private int _size;

    
    /**
     * Constructs an empty Stack.
     */
    public ArrayStringStack() {
        _stringStack = new String[5];
        _size = 0;
    }
        
    
    /**
     * Gets Size.
     */
    public int getSize(){
        return _size;
    }
    
    
    /**
     * Returns true if stack is empty, false otherwise.
     * @return whether stack is empty
     */
    public boolean isEmpty() {
        if(_size == 0)
            return true;
        return false;
    }

    
    /**
     * Pushes specified string onto the top of the stack.
     * @param s the string to be pushed onto the stack
     */
    public void push(String s){
            if (_size == _stringStack.length) {
                String[] temp = new String[_stringStack.length * 2];
                for (int i = 0; i < _stringStack.length; i++) {
                    temp[i] = _stringStack[i];
                }
                _stringStack = temp;
            }
            _stringStack[_size] = s;
            _size++;
    }

    
    /**
     * Removes and returns string currently sitting on top of stack,
     * assuming stack is not empty. (If it is empty, it generates a
     * "stack underflow" error.)
     * @return string on top of stack that is removed
     */
    public String pop(){
        if(_size == 0)
            throw new RuntimeException("Stack is Empty!"); 
        String s = _stringStack[_size-1];
        _size--;
        return s;
    }
    
}
