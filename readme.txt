Data Structures & Algorithms - Spring 2018
==========================================

## Programming Assignment 1: Stack and Linked Lists
### due date: Friday, February 9 at 5pm


-----------------------------------------------------------------

### Instructions

* Work on your own.
* Before writing any code, read through all the files in this
  archive.
* Unless otherwise indicated, use only the Java operations,
  commands, and standard library features  we have been using
  thus far in class and lab. (If you are unsure whether you can
  use something, just ask.) 
* Submit your work by creating a GitLab project, committing
  and pushing your work to that repository, sharing the
  repository with me (mbsiff) and emailing me a link to the
  repository upon your final submission. 


-----------------------------------------------------------------

### The problems to completed

1. Complete the class ArrayStringStack so that is correctly implements
   stacks of strings using resizable arrays directly rather than
   using the fully realized StringList class we developed in
   class and lab. Make your code as simple as possible rather
   than simply reusing entire chunks of our StringList code. You
   can test your stack with StringStackTest. 

2. Implement HTML validation as described in a previous email: we
   say a text file is valid HTML if every starting tag of the
   form <...> has a corresponding ending tag of the form </...>
   and if each ending tag corresponds to the most recent starting
   tag. All text outside of tags can safely be ignored. Work
   inside the Validate class and, specifically, the getTags and
   isValid functions in that class. Test your code by running
   Validate.main and giving it a filename to test for HTML
   validity. The two previously supplied examples are included in
   this archive. But be sure to create and test other examples.

3. Complete the class LinkedIntStack so that it correctly
   implements stacks of integers (corresponding to
   IntStackInterface) using an inner class representation of
   linked-list nodes. Again, keep your implementation as clean
   and simple as possible. However, implement an additional
   methods called stringRep (that should have "package" access -
   meaning it should be declared neither private nor public) that
   returns a String representation of your stack. (The string
   representation should be simpler than the usual list
   representation - show the contents of the stack from top to
   bottom, space separated.) Test your code with IntStackTest.

4. Implement a postfix calculator. Work within the evaluate function
   inside the Evaluate class. Your postfix calculator should
   handle the five standard integer arithmetic operators: +, -,
   *, / and %. And it should handle these additional commands:
   clr, dup, ifz, prn, and swp that behave as follows: 

       clr: resets the stack so that it consists only of a 0 on top.
       dup: duplicates the top of the stack.
       prn: prints out the current contents of the stack.
       swp: exchanges the top two items on the stack.

       ifz: replaces the top three elements with a single item
            that is either the current top of the stack (if the
            third from top is zero) or the second from top (if
            the third from the top is nonzero). For example, if
            the stack looked like:             <| 5 16 1 ... <|
            since 1 is not 0, it should replace the top three
            items with just 16 to look like:   <| 16 ... <|
            If the stack looked like:          <| 5 16 0 ... <|
            since 0 is third from top, it should replace the top
            items with just 5 to look like:    <| 5 ... <|


-----------------------------------------------------------------

Challenge problems available upon request.
