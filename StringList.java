/**
 * Lists of strings using resizable arrays.
 * 
 * @author Michael Siff
 */

public class StringList implements StringListInterface {
    private static final int INITIAL_CAPACITY = 2;   // unrealistically small
    private static final int GROWTH_MULTIPLIER = 2;
    private String[] _array;
    private int _size;

    
    public StringList() {
        _array = new String[INITIAL_CAPACITY];
        _size = 0;
    }


    public boolean isEmpty() {
        return _size == 0;
    }


    public int size() {
        return _size;
    }


    public String get(int index) {
        if (index >= 0 && index < _size) {
            return _array[index];
        } else {
            throw new RuntimeException("invalid list index: " + index);
        }
    }

    
    public void set(int index, String element) {
        if (index >= 0 && index < _size) {
            _array[index] = element;
        } else {
            throw new RuntimeException("invalid list index: " + index);
        }
    }

    
    public void add(String element) {
        add(_size, element);
    }


    public void add(int index, String element) {
        if (index >= 0 && index <= _size) {
            if (_size == _array.length) {
                String[] newArray = new String[_array.length * 2];
                for (int i = 0; i < _array.length; i++) {
                    newArray[i] = _array[i];
                }
                _array = newArray;
            }
            for (int i = _size; i > index; i--) {
                _array[i] = _array[i-1];
            }
            _array[index] = element;
            _size++;
        } else {
            throw new RuntimeException("invalid list index: " + index);
        }
    }

    
    public boolean contains(String element) {
        return indexOf(element) != -1;
    }


    public int indexOf(String element) {
        int pos = -1;
        int i = 0;
        while (pos == -1 && i < _size) {
            if (_array[i] == element) {
                pos = i;
            } else {
                i++;
            }
        }
        return pos;
    }


    public void clear() {
        _size = 0;
    }


    public String remove(int index) {
        if (index >= 0 && index < _size) {
            String element = _array[index];
            _size--;
            for (int i = index; i < _size; i++) {
                _array[i] = _array[i+1];
            }
            return element;
        } else {
            throw new RuntimeException("invalid list index: " + index);
        }
    }


    public String toString() {
        String s = "[";
        if (_size > 0) {
            s += _array[0];
            for (int i = 1; i < _size; i++) {
                s += ", ";
                s += _array[i];
            }
        }
        s += "]";
        return s;
    }
    
}
