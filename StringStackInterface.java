/**
 * Interface for stacks of strings.
 * 
 * @author Michael Siff
 */
public interface StringStackInterface
{

    /**
     * Returns true if stack is empty, false otherwise.
     * @return whether stack is empty
     */
    boolean isEmpty();

    
    /**
     * Pushes specified string onto top of stack.
     * @param s string to be pushed onto stack
     */
    void push(String s);

    
    /**
     * Removes and returns string currently sitting on top of stack,
     * assuming stack is not empty. (If empty, generates "stack
     * underflow" error.)
     * @return string that had been on top of stack
     */
    String pop();
    
}
