// Data Structures & Algorithms
// Spring 2018
// HW1: stacks and linked lists
// testing stacks of integers


import java.util.Random;

public class IntStackTest {
    public static void main(String[] args)  {
        final int N = 20;
        Random randomizer = new Random();
        LinkedIntStack stack = new LinkedIntStack();
        for (int i = 0; i < N; i++) {
            int r = randomizer.nextInt(N);
            System.out.print("  " + r);
            stack.push(r);
        }            
        System.out.println("\nstack: " + stack.stringRep());
        while (!stack.isEmpty()) {
            System.out.print("  " + stack.pop());
        }            
        System.out.println("\nfinal stack: " + stack.stringRep());
    }
}
