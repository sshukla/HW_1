// Data Structures & Algorithms
// Spring 2018
// HW1: stacks and linked lists
// testing stacks of strings


import java.util.Scanner;


public class StringStackTest {
    public static void main(String[] args) {
        String s;
        Scanner scanner = new Scanner(System.in);
        StringStackInterface stack = new ArrayStringStack();

        do {
            s = scanner.nextLine();
            if (s.length() > 1) {
                stack.push(s);
            }
        } while (s.length() > 1);

        System.out.println("\nreversed output:\n");
        
        while (!stack.isEmpty()) {
            System.out.println(stack.pop());
        }
    }
}
