/**
 * Interface for stacks of integers.
 * 
 * @author Michael Siff
 */
public interface IntStackInterface
{

    /**
     * Returns true if stack is empty, false otherwise.
     * @return whether stack is empty
     */
    boolean isEmpty();

    
    /**
     * Pushes specified integer onto top of stack.
     * @param i integer to be pushed onto stack
     */
    void push(int i);

    
    /**
     * Removes and returns integer currently sitting on top of stack,
     * assuming stack is not empty. (If empty, generates "stack
     * underflow" error.)
     * @return integer that had been on top of stack
     */
    int pop();
    
}
