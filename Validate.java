// Data Structures & Algorithms
// Spring 2018
// HW1: stacks and linked lists
// HTML validator (starter code)


import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;


/**
 * @author Shail Shukla
 */
public class Validate {

    public static void main(String[] args) throws IOException {
        // read contents of first argument into string
        String s = new String(Files.readAllBytes(Paths.get(args[0])));
        StringListInterface list = getTags(s);
        // System.out.println("" + list); // uncomment for testing
        if (isValid(list)) {
            System.out.println("Valid.");
        } else {
            System.out.println("Invalid!");
        }
    }


    /**
     * Returns a list of the HTML tags occurring in the source string.
     *
     * Example: getTags("<li>This <i>italicized</b> words") should
     * return ["<li>", "<i>", "</b>"].
     *
     * @param source string to be parsed into tags
     * @return list of tags occurring in order they occur in source
     */
    public static StringListInterface getTags(String source) {
        StringListInterface list = new StringList();
        String tag = "";
        boolean inTag = false;
        for (int i = 0; i < source.length(); i++) {
            char c = source.charAt(i);
            if (inTag) {
                tag += c;  // append character to the tag we are building up
                if (c == '>') {
                    list.add(tag);
                    inTag=false;
                }
            } else if (!inTag && c == '<') {
                tag = "<";
                inTag = true;
            }
        }
        return list;
    }
    

    /**
     * Returns true is the list of tags are valid: every opening tag is
     * followed, eventually, by a corresponding closing tag, and every
     * closing tag matches the most recently occuring opening tag.
     *
     * Examples:
     *    ["<li>", "<i>", "</b>"] ==> false
     *    ["<li>", "</li>", "<b>"] ==> false
     *    ["<li>", "<i>", "</i>", "</li>"] ==> true
     *
     * @param tags list of tags to be analyzed for validity
     * @return whether tags represent valid HTML
     */
    public static boolean isValid(StringListInterface tags) {
        StringStackInterface stack = new ArrayStringStack();
        boolean isValid = true;
        int size = tags.size();
        if(tags.size()%2 != 0)
            return false;
        if(tags.get(0).indexOf('/') != -1)
            return false;
        for(int i =0; i<size;i++){
            if(tags.get(i).indexOf('/') == -1){
                stack.push(tags.get(i));
            }else{
                String s = stack.pop();
                if(s.substring(1).equals(tags.get(i).substring(2)))
                    isValid = true;
                else
                    return false;
            }
        }
        return isValid;
    }
}
